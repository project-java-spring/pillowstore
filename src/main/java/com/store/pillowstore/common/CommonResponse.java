package com.store.pillowstore.common;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class CommonResponse {
    private String statusCode;
    private String message;
}
