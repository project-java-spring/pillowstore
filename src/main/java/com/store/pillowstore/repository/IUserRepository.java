package com.store.pillowstore.repository;

import com.store.pillowstore.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserRepository  extends JpaRepository<User,Long> {

}
