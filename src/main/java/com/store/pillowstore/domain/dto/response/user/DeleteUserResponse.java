package com.store.pillowstore.domain.dto.response.user;

import com.store.pillowstore.common.CommonResponse;
import com.store.pillowstore.constant.ResponseConstant.*;
import com.store.pillowstore.domain.User;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DeleteUserResponse extends CommonResponse {

    private User user;
    public DeleteUserResponse(User user) {
        super(StatusCodeResponse.Delete.CODE, StatusCodeResponse.Delete.MSG);
        this.user = user;
    }
}
