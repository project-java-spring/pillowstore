package com.store.pillowstore.domain.dto.request;


import com.store.pillowstore.domain.UserType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRequest {
    private String email;
    private String password;
    private UserType userType;
}
