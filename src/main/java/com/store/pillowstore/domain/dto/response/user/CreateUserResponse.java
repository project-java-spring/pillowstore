package com.store.pillowstore.domain.dto.response.user;

import com.store.pillowstore.common.CommonResponse;
import com.store.pillowstore.constant.ResponseConstant.StatusCodeResponse;
import com.store.pillowstore.domain.User;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CreateUserResponse extends CommonResponse {

    private User user;

    public CreateUserResponse(User user) {
        super(StatusCodeResponse.Success.CODE, StatusCodeResponse.Success.MSG);
        this.user = user;
    }
}
