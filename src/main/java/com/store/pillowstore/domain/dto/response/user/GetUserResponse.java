package com.store.pillowstore.domain.dto.response.user;


import com.store.pillowstore.common.CommonResponse;
import com.store.pillowstore.constant.ResponseConstant;
import com.store.pillowstore.domain.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetUserResponse extends CommonResponse {

    private User user;

    public GetUserResponse(User user) {
        super(ResponseConstant.StatusCodeResponse.Success.CODE, ResponseConstant.StatusCodeResponse.Success.MSG);
        this.user = user;
    }
}
