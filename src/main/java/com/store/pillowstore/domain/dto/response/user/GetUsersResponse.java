package com.store.pillowstore.domain.dto.response.user;

import com.store.pillowstore.common.CommonResponse;
import com.store.pillowstore.constant.ResponseConstant.*;
import com.store.pillowstore.domain.User;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class GetUsersResponse extends CommonResponse {

    List<User> userList;

    public GetUsersResponse(List<User> userList) {
        super(StatusCodeResponse.Success.CODE, StatusCodeResponse.Success.MSG);
        this.userList = userList;
    }
}
