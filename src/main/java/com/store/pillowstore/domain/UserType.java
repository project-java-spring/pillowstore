package com.store.pillowstore.domain;

public enum UserType {
    ADMINISTRATOR,
    USER
}
