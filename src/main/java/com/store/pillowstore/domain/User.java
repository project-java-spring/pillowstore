package com.store.pillowstore.domain;


import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
@Entity
@Table(name = Constants.Usertable.NAME)
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = Constants.Usertable.Id.NAME)
  private long Id;

  @Column(name = Constants.Usertable.Email.NAME,
          length = Constants.Usertable.Email.LENGTH,
          nullable = false)
  private String email;

  @Column(name=Constants.Usertable.Password.NAME,
          length = Constants.Usertable.Password.LENGHT
  )
  private String password;

  @Enumerated(EnumType.STRING)
  @Column(name = Constants.Usertable.UserType.NAME,
  length = Constants.Usertable.UserType.LENGTH
  )
  private UserType userType;

  @Temporal(TemporalType.DATE)
  @Column(name = Constants.Usertable.CreateDate.NAME
  )
  private Date createDate;

    @Temporal(TemporalType.DATE)
    @Column(name = Constants.Usertable.UpdateDate.NAME
    )
  private Date UpdateDate;

  @PrePersist
  void onPrepersist(){
      this.createDate = new Date();
  }
}
