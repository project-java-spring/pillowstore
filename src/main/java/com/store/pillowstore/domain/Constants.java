package com.store.pillowstore.domain;

public class Constants {
    static  class   Usertable{
        static final String NAME = "User_table";
        static class Id{
            static final String NAME="usr_id";
        }
        static class Email{
            static final String NAME="usr_email";
            static final int LENGTH = 100;
        }

        static class Password{
            static final String NAME = "usr_password";
            static final int LENGHT = 200;
        }

        static class UserType{
            static final String NAME = "usr_type";
            static final int LENGTH = 50;
        }
        static class CreateDate{
            static final String NAME = "usr_created_date";
        }
        static class UpdateDate{
            static final String NAME = "usr_update_date";
        }
    }
}
