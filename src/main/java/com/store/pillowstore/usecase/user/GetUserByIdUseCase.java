package com.store.pillowstore.usecase.user;

import com.store.pillowstore.domain.*;
import com.store.pillowstore.domain.dto.response.user.*;
import com.store.pillowstore.service.IUserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GetUserByIdUseCase {

    @Autowired
    private IUserServices userServices;

    public GetUserResponse execute(Long id){
        User user = userServices.getUserById(id);
        return buildGetUserByIdResponse(user);
    }

    private GetUserResponse buildGetUserByIdResponse(User user){
        return new GetUserResponse(user);
    }

}
