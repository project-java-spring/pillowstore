package com.store.pillowstore.usecase.user;

import com.store.pillowstore.domain.User;
import com.store.pillowstore.domain.dto.response.user.DeleteUserResponse;
import com.store.pillowstore.service.IUserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeleteUserByIdUseCase {

    @Autowired
    private IUserServices userServices;

    public DeleteUserResponse execute(Long id){
        User userDeleted = userServices.getUserById(id);
        userServices.deleteById(id);
        return new DeleteUserResponse(userDeleted);

    }
}
