package com.store.pillowstore.usecase.user;

import com.store.pillowstore.domain.User;
import com.store.pillowstore.domain.dto.request.UserRequest;
import com.store.pillowstore.domain.dto.response.user.CreateUserResponse;
import com.store.pillowstore.service.IUserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class CreateUserUseCase {

    @Autowired
    private IUserServices userService;

    public CreateUserResponse execute(UserRequest userRequest){
       // validateUser(userRequest);
        User user = userService.save(buildUser(userRequest));
        return buildUserResponse(user);
    }

    private User buildUser(UserRequest userRequest) {
        User user = new User();
        user.setEmail(userRequest.getEmail());
        user.setPassword(userRequest.getPassword());
        user.setUserType(userRequest.getUserType());
        user.setUpdateDate(new Date());
        return user;
    }

    private CreateUserResponse buildUserResponse(User user) {
        return new CreateUserResponse( user);
    }

}
