package com.store.pillowstore.usecase.user;

import com.store.pillowstore.domain.User;
import com.store.pillowstore.domain.dto.response.user.*;
import com.store.pillowstore.service.IUserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GetAllUsersUseCase {

    @Autowired
    private IUserServices userServices;

    public GetUsersResponse execute(){
        List<User> userList = userServices.getAll();
        return new GetUsersResponse(userList);
    }

}
