package com.store.pillowstore.service;

import com.store.pillowstore.domain.User;
import com.store.pillowstore.repository.IUserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements IUserServices {

    private IUserRepository userRepository;

    public UserServiceImpl(IUserRepository userRepository){
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public User getUserById(Long Id) {
        return userRepository.findById(Id).orElse(null);
    }

    @Override
    public void deleteById(Long Id) {
        userRepository.deleteById(Id);
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }
}
