package com.store.pillowstore.service;

import com.store.pillowstore.domain.User;

import java.util.List;

public interface IUserServices {
    List<User> getAll();
    User getUserById(Long Id);
    void deleteById(Long Id);
    User save(User user);
}
