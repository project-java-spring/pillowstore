package com.store.pillowstore.controller;

import com.store.pillowstore.domain.dto.request.UserRequest;
import com.store.pillowstore.domain.dto.response.user.*;

import com.store.pillowstore.usecase.user.CreateUserUseCase;
import com.store.pillowstore.usecase.user.DeleteUserByIdUseCase;
import com.store.pillowstore.usecase.user.GetAllUsersUseCase;
import com.store.pillowstore.usecase.user.GetUserByIdUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private CreateUserUseCase createUserUseCase;

    @Autowired
    private GetUserByIdUseCase getUserByIdUseCase;

    @Autowired
    private GetAllUsersUseCase getAllUsersUseCase;

    @Autowired
    private DeleteUserByIdUseCase deleteUserByIdUseCase;

    @PostMapping(value = "")
    public CreateUserResponse saveUser (@RequestBody UserRequest userRequest){
        return createUserUseCase.execute(userRequest);
    }

    @GetMapping(value = "/{userId}")
    public GetUserResponse getUser (@PathVariable("userId") Long id){
        return getUserByIdUseCase.execute(id);
    }

    @GetMapping
    public GetUsersResponse getAll(){
        return getAllUsersUseCase.execute();
    }

    @DeleteMapping(value = "/{userId}")
    public DeleteUserResponse deleteUser(@PathVariable("userId") Long id){
        return deleteUserByIdUseCase.execute(id);
    }
}
