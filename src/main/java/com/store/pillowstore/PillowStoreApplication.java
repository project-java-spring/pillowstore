package com.store.pillowstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PillowStoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(PillowStoreApplication.class, args);
    }

}
