package com.store.pillowstore.constant;

public final class ResponseConstant {

    public static class StatusCodeResponse{

        public static class Success{
            public static final String CODE = "200";
            public static final String MSG = "Operation Success";
        }

        public static class Delete{
            public static final String CODE = "201";
            public static final String MSG = "Delete Success";
        }

        public static class NotFound{
            public static final String CODE = "500";
            public static final String MSG = "Item not found";
        }
        private StatusCodeResponse() {
            throw new IllegalStateException("Failed");
        }
    }





}
